public class Regex {
    public static void main(String[] args) {
        String s = "Hello World";
        System.out.println(s.matches("Hello World"));
        System.out.println(s.matches("Hello World!"));

        System.out.println(s.matches(".+"));

        String one = " ";
        System.out.println(one.matches("."));

        String points = "...";
        System.out.println(points.matches("\\.+"));

        String letters = "ababababcccabc";
        System.out.println(letters.matches("[abc]+"));

        String TicTacToeMove = "2,1";

        System.out.println(TicTacToeMove.matches("[0-2],[0-2]"));

        String Chess = "A1";
        System.out.println(Chess.matches("[a-zA-Z][0-9]"));


        String firstName = "Andrei-George-Costel";
        System.out.println(firstName.matches("([A-Z][a-z]+|[ -])+[a-z]"));

        String emailAdress = "balasca.andrei@gmail.co.uk";
        System.out.println(emailAdress.matches("[a-zA-Z0-9\\.-]{3,}@{1}[a-zA-Z0-9-]{3,}(\\.{1}[a-z]{2,}){1,2}"));

    }
}
